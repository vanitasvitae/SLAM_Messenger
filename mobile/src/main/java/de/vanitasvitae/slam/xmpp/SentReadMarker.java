package de.vanitasvitae.slam.xmpp;

/**
 * Created by Paul Schaub on 23.02.18.
 */
public enum SentReadMarker {
    sending,
    sent,
    read
}
